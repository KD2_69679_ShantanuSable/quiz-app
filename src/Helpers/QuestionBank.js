export const Question=[
    {
        prompt: "what is the chemical formula of dry-ice",
        optionA : "CO2",
        optionB : "H2O",
        optionC : "Ch4",
        optionD : "O2",
        Answer : "A"
    },
    {
        prompt: "what is square-root of 196",
        optionA : "12",
        optionB : "13",
        optionC : "14",
        optionD : "16",
        Answer : "C"
    },
    {
        prompt: "which is a streaming platform",
        optionA : "twitter",
        optionB : "twich",
        optionC : "Robbinhood",
        optionD : "Google",
        Answer : "B"
    }
]