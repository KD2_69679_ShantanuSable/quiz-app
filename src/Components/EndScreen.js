import React, { useContext } from 'react'
import { QuizContext } from '../Helpers/Contexts'

export default function EndScreen() {
  const {score,setScore}=useContext(QuizContext);
  alert("quiz completed")
  return (
    <div className='EndScreen'>
      <h1> your score is {score} out of 3</h1>
    </div>
  )
}
