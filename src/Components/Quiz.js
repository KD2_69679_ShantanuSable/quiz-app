import React, { useState } from 'react'
import '../App.css'
import { QuizContext } from '../Helpers/Contexts';
import { useContext } from 'react';
import { Question, QuestionBank } from '../Helpers/QuestionBank';

function Quiz() {
  const {gameState,setgameState}=useContext(QuizContext);
  const {score,setScore}=useContext(QuizContext);
  const [curruntQuestion,setCurrentQuestion]=useState(0);
  const [optionChosen,setOptionChosen]=useState("");
  
  const nextQuestion=()=>{
    if(optionChosen === Question[curruntQuestion].Answer){
      setScore(score+1);
    }
    setCurrentQuestion(curruntQuestion+1)
  }
  const FinishQuiz=()=>{
    if(optionChosen === Question[curruntQuestion].Answer){
      setScore(score+1);
    }
    setgameState("end screen");
  }
  const [style, setStyle] = useState("light");
 
  return (
    <div className='Quiz'>
      <div className='option'>
    <h3>{Question[curruntQuestion].prompt}</h3>
    <button onClick={()=>{setOptionChosen("A")}}>{Question[curruntQuestion].optionA}</button>
    <button onClick={()=>{setOptionChosen("B")}}>{Question[curruntQuestion].optionB}</button>
    <button onClick={()=>{setOptionChosen("C")}}>{Question[curruntQuestion].optionC}</button>
    <button onClick={()=>{setOptionChosen("D")}}>{Question[curruntQuestion].optionD}</button>
    </div>
    {curruntQuestion===Question.length-1 ? 
     (<button onClick={FinishQuiz}>Finish Quiz</button>) :
     (<button onClick={nextQuestion}>Next</button>)
    }
    </div>
    
  ) ;}

export default Quiz; 