import { useState } from "react";
import MainMenu from "./Components/MainMenu";
import Quiz from "./Components/Quiz";
import EndScreen from "./Components/EndScreen";
import { QuizContext } from "./Helpers/Contexts";
import './App.css'

function App() {
  const[gameState,setgameState]=useState("menu");
  const [score,setScore]=useState(0);
  return (
    <div className="App">
      <h1>welcome</h1>  
      <QuizContext.Provider value={{gameState,setgameState,score,setScore}}>
      {gameState === "menu" && <MainMenu />}
      {gameState === "quiz" && <Quiz />}
      {gameState === "end screen" && <EndScreen />}
      </QuizContext.Provider> 
    </div>
  );
}
export default App;
